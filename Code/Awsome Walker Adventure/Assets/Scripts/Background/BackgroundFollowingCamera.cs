﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundFollowingCamera : MonoBehaviour
{
    [SerializeField]
    private Camera camera;
    private float ratioParallax = 1.4f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Follow();
    }

  

    private void Follow()
    {
        transform.position = new Vector3(camera.transform.position.x/ ratioParallax, camera.transform.position.y/ ratioParallax, transform.position.z) ;
    }
}
