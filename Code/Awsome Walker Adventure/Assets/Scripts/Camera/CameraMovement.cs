﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    private Camera camera;

    private Vector3 dragOrigin;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MoveCamera();
    }


    private void LateUpdate()
    {
        AutoMoveCamera(0.1f);
    }

    private void AutoMoveCamera(float v)
    {
        camera.transform.position += Vector3.right * v;
        //camera.transform.Rotate(0, v, 0);
    }

    private void MoveCamera()
    {
        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = camera.ScreenToWorldPoint(Input.mousePosition); 
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 diff = dragOrigin - camera.ScreenToWorldPoint(Input.mousePosition);
            camera.transform.position += diff;
        }


    }

}
